from channels.generic.websocket import AsyncWebsocketConsumer
import json
import os
from django.conf import settings

from diff_match_patch import diff_match_patch


class CodeEditorConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = f'code_editor_{self.room_name}'

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        filename = text_data_json['filename']
        diffs = text_data_json['diffs']  # Получение дельт вместо полного контента

        try:
            # Обновление файла с использованием дельт
            await self.apply_diffs_to_file(filename, diffs)
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'send_update',
                    'filename': filename,
                    'diffs': diffs
                }
            )
        except Exception as e:
            print(f"Error updating file: {e}")

    async def send_update(self, event):
        # Отправка дельт обратно всем подключенным клиентам
        await self.send(text_data=json.dumps({
            'filename': event['filename'],
            'diffs': event['diffs']
        }))

    async def apply_diffs_to_file(self, filename, diffs):
        # Открытие файла и применение дельт к его содержимому
        dmp = diff_match_patch()
        repo_directory = settings.REPO_DIRECTORY / self.room_name
        container_directory = repo_directory / filename
        with open(container_directory, 'r') as file:
            content = file.read()
        patches = dmp.patch_make(content, diffs)
        new_content, _ = dmp.patch_apply(patches, content)
        with open(container_directory, 'w') as file:
            file.write(new_content)
