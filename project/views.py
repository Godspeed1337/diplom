import traceback

from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from project.models import Room, Project
from project.serializers import RoomSerializer, ProjectSerializer
from project.utils.docker_git import create_container_with_repo, create_branch, remove_container_with_repo, \
    commit_and_push_changes
from django.shortcuts import get_object_or_404


class CreateProjectView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        serializer = ProjectSerializer(data=request.data)
        if serializer.is_valid():
            project = serializer.save(user=request.user)
            return Response({"message": "Project created successfully", "project_id": project.id})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CreateRoomView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        serializer = RoomSerializer(data=request.data)
        if serializer.is_valid():
            project = serializer.validated_data['project']
            room_name = serializer.validated_data['name'].replace(' ', '_')
            branch_name = f"{room_name}-branch"

            try:
                container = create_container_with_repo(project.git_url, project.ssh_key, room_name, project.token)
                create_branch(room_name, branch_name)
                room = serializer.save(name=room_name, branch_name=branch_name)
                return Response({"message": "Room created successfully", "room_id": room.id})
            except Exception as e:
                # return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                remove_container_with_repo(room_name)
                raise
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RoomDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Room.objects.all()
    serializer_class = RoomSerializer

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            remove_container_with_repo(instance.name)
            instance.delete()
            return Response({"message": "Room and associated container deleted successfully"}, status=status.HTTP_204_NO_CONTENT)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class RoomListView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Room.objects.all()
    serializer_class = RoomSerializer

    def get_queryset(self):
        user = self.request.user
        return Room.objects.filter(project__user=user)


class RoomUpdateView(generics.UpdateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Room.objects.all()
    serializer_class = RoomSerializer


class CommitChangesView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        room_name = request.data.get('room_name')
        commit_message = request.data.get('commit_message')

        if not room_name or not commit_message:
            return Response({"error": "room_name and commit_message are required"}, status=status.HTTP_400_BAD_REQUEST)

        room = get_object_or_404(Room, name=room_name)
        project = room.project

        try:
            commit_and_push_changes(room_name, commit_message, project.token, project.ssh_key)
            return Response({"message": "Changes committed and pushed successfully"})
        except Exception as e:
            print(f"\t {traceback.format_exc()}")
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
