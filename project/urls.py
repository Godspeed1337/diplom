from django.urls import path
from .views import CreateProjectView, CreateRoomView, RoomListView, RoomDetailView, RoomUpdateView, CommitChangesView

urlpatterns = [
    path('create-project/', CreateProjectView.as_view(), name='create-project'),
    path('create-room/', CreateRoomView.as_view(), name='create-room'),
    path('rooms/', RoomListView.as_view(), name='room-list'),
    path('rooms/<int:pk>/', RoomDetailView.as_view(), name='room-detail'),
    path('rooms/<int:pk>/update/', RoomUpdateView.as_view(), name='room-update'),
    path('commit-changes/', CommitChangesView.as_view(), name='commit-changes'),
]