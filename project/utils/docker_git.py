import docker
from git import Repo
from django.conf import settings


def create_container_with_repo(git_url, ssh_key, room_name, token):
    client = docker.from_env()
    repo_directory = settings.REPO_DIRECTORY / room_name
    container_directory = "/workspace"  # директория в контейнере
    # Создание контейнера
    container = client.containers.run(
        "python:3.8",
        name=room_name,
        command="tail -f /dev/null",
        volumes={repo_directory: {'bind': container_directory, 'mode': 'rw'}},
        working_dir=container_directory,
        detach=True
    )

    # Клонирование репозитория в директорию в контейнере
    clone_repository(git_url, ssh_key, token, container, container_directory)

    return container


def clone_repository(git_url, ssh_key, token, container, directory):
    if ssh_key:
        cmd = f"git clone {git_url} ."
        exec_result = container.exec_run(
            f"GIT_SSH_COMMAND='ssh -i {ssh_key} -o StrictHostKeyChecking=no' {cmd}")
    elif token:
        cmd = f"git clone https://{token}@{git_url.replace('https://', '')} ."
        exec_result = container.exec_run(cmd)
    else:
        cmd = f"git clone {git_url} ."
        exec_result = container.exec_run(cmd)
    print(exec_result.output.decode())
    print("---")


def create_branch(room_name, branch_name):
    repo_directory = settings.REPO_DIRECTORY / room_name
    repo = Repo(str(repo_directory))
    new_branch = repo.create_head(branch_name)
    new_branch.checkout()


def commit_and_push_changes(room_name, commit_message, token=None, ssh_key=None):
    repo_directory = settings.REPO_DIRECTORY / room_name
    repo = Repo(str(repo_directory))
    repo.git.add('--all')
    repo.index.commit(commit_message)
    origin = repo.remote(name='origin')
    if ssh_key:
        repo.git.update_environment(GIT_SSH_COMMAND=f'ssh -i {ssh_key} -o StrictHostKeyChecking=no')

    repo.git.push('--set-upstream', origin.name, repo.active_branch.name)

    origin.push()


def remove_container_with_repo(room_name):
    client = docker.from_env()
    containers = client.containers.list(all=True, filters={"name": room_name})
    for container in containers:
        container.stop()
        container.remove(v=True, force=True)
    repo_directory = settings.REPO_DIRECTORY / room_name
    if repo_directory.exists() and repo_directory.is_dir():
        try:
            repo_directory.rmdir()
        except PermissionError as e:
            print(f"Error removing directory: {e}")
