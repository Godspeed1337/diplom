from rest_framework import serializers
from .models import Project, Room


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'name', 'description', 'git_url', 'ssh_key', 'token', 'created_at')


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ('id', 'project', 'name', 'created_at')
