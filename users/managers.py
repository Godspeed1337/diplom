from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import gettext_lazy as _


class CustomUserManager(BaseUserManager):

    def create_user(self, first_name, email, password):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError(_('Электронная почта должна быть установлена'))
        # делаем доменную часть любого имейла в нижнем регистре (нормализуем)
        email = self.normalize_email(email)
        user = self.model(first_name=first_name, email=email, password=password)
        user.is_staff = False
        user.is_superuser = False

        # установка пароля пользователя, учитывая хэширование
        user.set_password(password)
        # сохранение объекта в модели
        user.save(using=self._db)
        return user

    def create_staffuser(self, first_name, email, password):
        """
        Creates and saves a staff user with the given email and password.
        """
        email = self.normalize_email(email)
        user = self.create_user(first_name=first_name, email=email, password=password)
        user.is_staff = True
        user.is_superuser = False
        user.save(using=self._db)
        return user

    def create_superuser(self, first_name, email, password):
        """
        Create and save a SuperUser with the given email and password.
        """
        email = self.normalize_email(email)
        user = self.create_user(first_name=first_name, email=email, password=password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user