from django.db import models
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser
from .managers import CustomUserManager


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(null=False, unique=True, max_length=150)
    first_name = models.CharField(blank=False, max_length=150, unique=False)
    last_name = models.CharField(blank=True, max_length=150, unique=False)
    is_staff = models.BooleanField(default=False, null=False)
    is_superuser = models.BooleanField(default=False, null=False)
    date_joined = models.DateTimeField(auto_now_add=True, null=False)

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'

    REQUIRED_FIELDS = ['first_name']

    objects = CustomUserManager()

    class Meta:
        db_table = 'users'

    def get_short_name(self):
        return self.first_name

    def natural_key(self):
        return self.email

    def __str__(self):
        return self.email
